-- Создать schema.sql файл, который содержит описание таблиц и данных для этих таблиц
--
-- Товар
-- - id
-- - описание
-- - стоимость
-- - количество
--
-- Заказчик
-- - id
-- - имя/фамилия
--
-- Заказ
-- - id-товара (внешний ключ)
-- - id-заказчика (внешний ключ)
-- - дата заказа
-- - количество товаров
--
-- Написать 3-4 запроса на эти таблицы.

create table product
(
    id     serial primary key,
    name   varchar(20),
    cost   integer,
    amount integer check (cost > 0)
);
create table customer
(
    id          serial primary key,
    first_name  varchar(20),
    second_name varchar(20)
);

create table "order"
(
    id_product   integer,
    id_customer  integer,
    data         date,
    amount_order integer check (amount_order > 0),
    foreign key (id_product) references product (id),
    foreign key (id_customer) references customer (id)
);

insert into product(name, cost, amount)
values ('Apple', 56, 100);
insert into product(name, cost, amount)
values ('Orange', 70, 20);
insert into product(name, cost, amount)
values ('Banana', 40, 30);

insert into customer(first_name, second_name)
values ('Evgeniy', 'Blednykh');
insert into customer(first_name, second_name)
values ('Tom', 'Volodarski');
insert into customer(first_name, second_name)
values ('Michael', 'Jordon');

insert into "order" (id_product, id_customer, data, amount_order)
values (1, 2, '2021-11-29', 10);
insert into "order" (id_product, id_customer, data, amount_order)
values (3, 1, '2021-11-29', 30);
insert into "order" (id_product, id_customer, data, amount_order)
values (1, 1, '2021-11-29', 30);
-- удалил лишних заказчиков
delete
from customer
where id > 3;

--- имена клиентов, которые сделали заказ
select distinct (first_name)
from customer a
         right join "order" o on a.id = o.id_customer;

--- что купили и в каком количество
select distinct (name), amount_order
from product
         right join "order" o on product.id = o.id_product;

---имя и фамилия всех и их связь с 3-й таблицей
select *
from customer
         full join "order" o on customer.id = o.id_customer;