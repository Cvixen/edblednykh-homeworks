import java.util.Arrays;

public class homework_06_2 {
    public static void swap(int[] array) {
        int repeat = -1;
        while (++repeat < array.length) {
            for (int i = 0; i < array.length - 1; i++)
                if (array[i] == 0) {
                    array[i] = array[i + 1];
                    array[i + 1] = 0;
                }
        }
    }
    public static void main(String[] args) {
        int[] array = {12,0,0,21,0,45,0,97,98};
        swap(array);
        System.out.println(Arrays.toString(array));
    }
}
