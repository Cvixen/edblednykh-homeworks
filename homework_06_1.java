public class homework_06_1 {
    public static int getIndex(int[] array, int digit) {
        for(int i = 0; i < array.length; i++) {
            if (array[i] == digit)
                return i;
        }
        return -1;
    }
    public static void main(String[] args) {
        int[]array = {12,17,19,21,22,45,47,97,98};
        int digit = 45;
        System.out.println(getIndex(array,digit));
    }
}
